+++
date = "1019-07-19"
tags = ["blog", "hugo"]
title = "Creating a blog using Hugo"
+++

I'm way overdue having my own blog so I decide today was a good day to start.  Since the site will be relatively simple, I decided to start with a static site generator.  Being a Go fan plus having some experience with Hugo, I decided Hugo was a good place to start.

I was able to get started in a few simple steps:  
1) Install Hugo  
2) Create a site  
3) Select and install a Theme  
4) Configure the site  
5) View the site  
6) Write some posts!  


#### Install Hugo
It is simple to install Hugo.  Just follow the installation instructions on [gohugo.io](https://gohugo.io/getting-started/installing/).  

You can test your install by running:

{{< highlight bash >}}
hugo version
{{< /highlight >}}

#### Create a site
With the hugo CLI, it only takes one command to create a new site.

{{< highlight bash >}}
hugo new site myblog.com
{{< /highlight >}}

#### Select and install a theme
I decided to go with a simple dark them called m10c.  To pick a different theme, visit [themes.gohugo.io](https://themes.gohugo.io/).  To install the theme, clone the theme to the themes directory.  
{{< highlight bash >}}
git clone https://github.com/vaga/hugo-theme-m10c.git themes/m10c
{{< /highlight >}}

#### Configure the site
When the site was generated, the generation step created a configuration file called "config.toml".  This file contains the configuration for the site and the theme.

{{< highlight toml >}}
baseURL = "http://example.org/" # Make sure you set this correctly to avoid css issues
languageCode = "en-us"
title = "My New Hugo Site"
{{< /highlight >}}

To see a complete listing of the configuration settings, visit [gohugo.io](https://gohugo.io/getting-started/configuration/#all-configuration-settings)

To use a theme from the themes directory, simply set the "theme" setting.
{{< highlight toml >}}
theme = "m10c"
{{< /highlight >}}


##### m10c specific settings
The settings below will personalize your blog.  The end result is a clean, dark themed blog page.

{{< highlight toml >}}
[params]
    author = "<your name>"
    description = "<description under your avatar>"
    avatar = "avatar.jpg" # Will pull from your static folder
    [[params.social]]
        name = "gitlab"
        url = "https://gitlab.com/<insert info>"
    [[params.social]]
        name = "github"
        url = "https://github.com/<insert info>"
    [[params.social]]
        name = "twitter"
        url = "https://twitter.com/<insert info>"
    [[params.social]]
        name = "linkedin"
        url = "https://linkedin.com/in/<insert info>"
{{< /highlight >}}

#### View the site
Hugo will serve up your site locally in one easy command.

{{< highlight toml >}}
hugo server -v -D # -v Verbose, -D include content marked as draft
{{< /highlight >}}

Once running, you can access the site locally at localhost:1313.  The server watches for changes and updates you site on the fly.

#### Write some posts!
"The first step is to say it poorly. And then say it again and again and again until you're able to edit your words into something that works." - [Seth Godin](https://seths.blog/2014/10/do-the-word/)

Fitting quote for my first blog in a number of years. 

To create a new post, use the hugo new CLI command.
{{< highlight toml >}}
hugo new posts/post1.md
{{< /highlight >}}

If you would like to customize how the initial post is created, alter "archetypes/default.md" to get your desired behavior.  One example is changing the default metadata from YAML to TOML.

## Wrap up
It was super easy to get started on my blog with Hugo.  I barely scratched the surface of all the available features. If you do not find Hugo appealing, there are many tools to help you create a blog site.  Pick one, and start writing.  
